<?php

require_once __DIR__ . '/vendor/autoload.php';

$data = ['name' => 'Vasili'];
$pdfReporter = new \App\PdfReporter();
$reportContent = $pdfReporter->getReportContent($data, 'hello');

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($reportContent);
$mpdf->Output();
